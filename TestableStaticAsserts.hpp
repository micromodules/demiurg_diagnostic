#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_diagnostic__Version

#include "demiurg_diagnostic/Check.hpp"

#ifdef DTesting_UseExceptionsModeForDStaticAsserts
#define DTestableStaticAssert(M_CheckResult, M_Message, M_Exception)\
    Check::staticCheckFunction<(M_CheckResult)>(M_Message, M_Exception);
#else
#define DTestableStaticAssert(M_CheckResult, M_Message, M_Exception)\
    static_assert((M_CheckResult), M_Message);
#endif

#endif
