#pragma once

namespace Check
{
    template<typename T_ExceptionType>
    inline void checkFunction(const bool inCondition, T_ExceptionType&& inException) {
        if (!inCondition)
            throw inException;
    }

    template<bool T_CheckResult, typename T_ExceptionType>
    void staticCheckFunction(const char* inMessage, T_ExceptionType&& inException) {
        if (!T_CheckResult) {
            inException.message = inMessage;
            throw inException;
        }
    }
}
