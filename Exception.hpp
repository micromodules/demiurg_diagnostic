#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_diagnostic__Version

namespace Check
{
    struct Exception {
		const char* message = nullptr;
	};
}

#endif
