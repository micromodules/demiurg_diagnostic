#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_diagnostic__Version

#include <iostream>
#include <iomanip>

inline void printDump(const DByteType* inMemory, const uint16_t inLinesToPrint, const uint8_t inBytesPerLine = 16) {
    std::cout << " -------------[ Memory dump ]----------------{" << std::endl;

    const DByteType* theByteToPrint = inMemory;

    for (uint16_t theLineIndex = 0; theLineIndex < inLinesToPrint; ++theLineIndex) {
        std::cout << reinterpret_cast<const void*>(theByteToPrint) << " | ";

        std::cout << std::hex << std::setw(2) << std::setfill('0') << std::uppercase <<  static_cast<int>(*(theByteToPrint++));
        for(uint16_t theByteIndexInLine = 1; theByteIndexInLine < inBytesPerLine; ++theByteIndexInLine) {
            std::cout << " " << std::hex << std::setw(2) << std::setfill('0') << std::uppercase << static_cast<int>(*(theByteToPrint++));
        }
        std::cout << std::endl;
    }

    std::cout << "}" << std::endl;
}

#endif
