#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_diagnostic__Version

//#define DTesting
#ifdef DTesting
#   define DTesting_UseExceptionsModeForDStaticAsserts
#endif

#include "details/Details.hpp"

#define DCheck(M_Condition, M_Exception)\
    Check::checkFunction((M_Condition), M_Exception)

#endif
